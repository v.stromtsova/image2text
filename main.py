import requests
import json
import base64
from pathlib import Path
from settings import IAM_TOKEN, FOLDER_ID

API_URL = 'https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze'
HEADERS = {
    'Content-Type': 'application/json',
    'Authorization': f'Bearer {IAM_TOKEN}'
}
payload = {
    "folderId": FOLDER_ID,
    "analyze_specs": [{
        "features": [{
            "type": "TEXT_DETECTION",
            "text_detection_config": {
                "language_codes": ["*"]
            }
        }]
    }]
}

FILE_TYPES = ('png', 'jpg', 'pdf')
MIME_TYPES = {'.png': 'image/png', '.jpg': 'image/jpeg', '.pdf': 'application/pdf'}


def find_images(src_dir):
    result = []
    for ft in FILE_TYPES:
        for path in Path(src_dir).rglob(f'*.{ft}'):
            result.append(path)
    return result


def encode_file(file):
    file_content = file.read()
    return base64.b64encode(file_content).decode("utf-8")


def get_text(source_image):
    with open(source_image, 'rb') as file:
        content = encode_file(file)
    payload["analyze_specs"][0]["content"] = content
    payload["analyze_specs"][0]["mime_type"] = MIME_TYPES[source_image.suffix]

    r = requests.post(
        API_URL,
        headers=HEADERS,
        data=json.dumps(payload),
    )
    parsed_text = parse_response(json.loads(r.text))
    return parsed_text


def parse_response(response):
    result = []
    for block in response['results'][0]['results'][0]['textDetection']['pages'][0]['blocks']:  # OMG
        for line in block['lines']:
            for word in line['words']:
                result.append(word['text'])
    return result


if __name__ == "__main__":
    text_by_image = dict()
    for image in find_images(input("Enter path to directory with images: ")):
        text = get_text(image)
        text_by_image[image] = text
    with open("result.txt", "w") as file:
        for key, value in text_by_image.items():
            file.write(f"{key}\n")
            file.write(f"{' '.join(value)}\n")
            file.write("\n")
