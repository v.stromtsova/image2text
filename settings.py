from dotenv import load_dotenv
import os

load_dotenv()
OAUTH_TOKEN = os.getenv('OAUTH_TOKEN')
IAM_TOKEN = os.getenv('IAM_TOKEN')
FOLDER_ID = os.getenv('FOLDER_ID')
