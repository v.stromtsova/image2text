# image2text

## How to run
- Add an `.env` file to the root folder of the project. 
Define the vollowing env variables:
```
OAUTH_TOKEN=<Yandex cloud OAuth token>
FOLDER_ID=<Yandex cloud catalogue id>
```
- Create a virtualenv and run `pip install -r requirements.txt`
- Run `python main.py` and enter the path to desired source directory